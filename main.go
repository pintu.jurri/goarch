package main

import (
    "./http"
)

func main() {
	
	// http.HandleFunc("/", sayhelloName) // set router
	// err := http.ListenAndServe(":9090", nil) // set listen port
	// if err != nil {
	//     log.Fatal("ListenAndServe: ", err)
	// }
	http.RunHttp()

}
